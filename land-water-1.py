# Land or water problem 1.
import numpy as np
import matplotlib.pyplot as plt
import math

# The data is in int8 format.
map_data = np.fromfile("gl-latlong-1km-landcover.bsq", dtype=np.int8)


# Now, the map_data is one long list. We want to divide it into
# a pixel map. Each row of the array is a row of pixels.
map_data = np.reshape(map_data, (43200, 21600))

# Looking at some random pixels of the world
map_data[11000:11006]

# Looking at the subsample of the map.
plt.imshow(map_data[::50, ::50])

# The following takes coordinates and tells you whether it's
# land or water.


def coordinator(x, y):
    "Takes coordinates and turns them into pixel locations"
    new_x = ((x + 180) / 360 * 43199)
    new_y = ((y + 90) / 180 * 21599)
    return(math.floor(new_x), math.floor(new_y))


def water_finder(x, y):
    "Takes coordinates and tells you whether it's water"
    longitude, latitude = coordinator(x, y)
    place = map_data[longitude, latitude]
    if place == 0:
        print("It's water! Ahoy!")
    else:
        print("It's land! Argh!")


# Testing the function
water_finder(0, 0)
water_finder(0.1, 0.1)

# This line should create a conflict later.

# Reading the earthquakes and printing coordinates
earthquakes_filename = 'events_4.5.txt'
with open(earthquakes_filename, 'r') as fo:
    for line in fo.readlines():
        if line[0] == '#':
            continue
        row = line.strip().split(';')
        latitude = float(row[1])
        longitude = float(row[2])

        print(f"Lat: {latitude:0.2f}, Long: {longitude:0.2f}, ", end='')
        msg = water_finder(longitude, latitude)

#The next step is Task 3: Plot options
